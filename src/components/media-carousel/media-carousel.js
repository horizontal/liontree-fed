
if (!$('.experience-editor').length) {
  var $status = $('#media-carousel');
  var $slickElement = $('.slideshow-media[data-id="media-carousel"]');

  $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    if(!slick.$dots) {
      return;
    }

    var i = (currentSlide ? currentSlide : 0) + 1,
        total = (slick.$dots[0].children.length);

    i = ("0" + i).slice(-2);
    total = ("0" + total).slice(-2);

    $status.text(i + ' / ' + total);
  });

  $slickElement.slick({
    infinite: true,
    autoplay: false,
    dots: true,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: '<button type="button" class="slick-next"></button>',
    prevArrow: '<button type="button" class="slick-prev"></button>',
    responsive: [{
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        nextArrow: '<button type="button" class="slick-next"></button>',
        prevArrow: '<button type="button" class="slick-prev"></button>',
        dots: true,
      }
    }]
  }).on('setPosition', function (event, slick) {
    slick.$slides.css('height', slick.$slideTrack.height() + 'px');
  });


  $('.slideshow-media').on('beforeChange', function() {
    // Pause video/audio upon slide change
    var videoColl = $('.slideshow-media').find('video'),
        audioColl = $('.slideshow-media').find('audio');

    $.each(videoColl, function(id, video) {
      video.pause();
    });

    $.each(audioColl, function(id, audio) {
      audio.pause();
    });
  });
};
