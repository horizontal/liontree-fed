if (!$('.experience-editor').length) {
  var $pager1 = $('#announcements-carousel');
  var $slickElement1 = $('.slideshow[data-id="announcements-carousel"]');

  $slickElement1.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    if(!slick.$dots) {
      return;
    }

    var i = (currentSlide ? currentSlide : 0) + 1,
        total = (slick.$dots[0].children.length);

    i = ("0" + i).slice(-2);
    total = ("0" + total).slice(-2);

    // $($slickElement).prev().text(i + ' / ' + total);
    $pager1.text(i + ' / ' + total);
  });

  console.log($($slickElement1).prev());

  $slickElement1.slick({
    infinite: true,
    autoplay: false,
    dots: true,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    nextArrow: '<button type="button" class="slick-next"></button>',
    prevArrow: '<button type="button" class="slick-prev"></button>',
    responsive: [{
      breakpoint: 1080,
      settings: {
        slidesToScroll: 2,
        slidesToShow: 2,
      }
    },{
      breakpoint: 767,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
        nextArrow: '<button type="button" class="slick-next"></button>',
        prevArrow: '<button type="button" class="slick-prev"></button>',
        dots: true,
      }
    }]
  });
}

if (!$('.experience-editor').length) {
  var $pager2 = $('#news-and-insights-carousel');
  var $slickElement2 = $('.slideshow[data-id="news-and-insights-carousel"]');

  $slickElement2.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    if(!slick.$dots) {
      return;
    }

    var i = (currentSlide ? currentSlide : 0) + 1,
        total = (slick.$dots[0].children.length);

    i = ("0" + i).slice(-2);
    total = ("0" + total).slice(-2);

    // $($slickElement).prev().text(i + ' / ' + total);
    $pager2.text(i + ' / ' + total);
  });

  console.log($($slickElement2).prev());

  $slickElement2.slick({
    infinite: true,
    autoplay: false,
    dots: true,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    nextArrow: '<button type="button" class="slick-next"></button>',
    prevArrow: '<button type="button" class="slick-prev"></button>',
    responsive: [{
      breakpoint: 1080,
      settings: {
        slidesToScroll: 2,
        slidesToShow: 2,
      }
    },{
      breakpoint: 767,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
        nextArrow: '<button type="button" class="slick-next"></button>',
        prevArrow: '<button type="button" class="slick-prev"></button>',
        dots: true,
      }
    }]
  });
}
