$(function() {
  var i = {
    get: function($) {
      try {
        var t = localStorage.getItem($);
        return t ? JSON.parse(t) : null
      } catch (i) {}
    },
    set: function($, t) {
      try {
        localStorage.setItem($, JSON.stringify(t))
      } catch (i) {}
    }
  };

  var isIE = window.ActiveXObject || "ActiveXObject" in window;
  if (isIE) {
    // A known bug prevents modals from working in IE
    // This fixes the issue by removing the offending "fade" class, but only if viewing with IE
    $('#cookieconsent').removeClass('fade');
  }

  if (i.get('cookie-consenttest2')) {
    // If cookie exists, remove the modal from the DOM
    $('#cookieconsent').remove();
  } else {
    $('#cookieconsent').toggleClass('show');
  }



  $('#cookie').click(function () {
    var $this = $(this),
        href = ($this.attr('href') || window.location) + '';

    $(this).closest('.modal').toggleClass('show');
    i.set('cookie-consenttest2', !0);

    $('#cookieconsent').modal('toggle');

    $.post("/api/CustomItem/TrackSiteAnalytics", {
      ItemID: "a02b9e30-53e9-4583-b54d-39988d66d21c",
      PageEvent: "PageView",
      Text: "Click",
      Data: "Clicked link - " + $this.text(),
      Url: href
    });
  });
});
