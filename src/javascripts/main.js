;(function ($, window, document, undefined) {

  var titlesColl = [].slice.call(document.querySelectorAll('.section-title a')),
      mobileMenu = $('#mobileMenu');

  $.each(titlesColl, function( index, value ) {
    var title = $(value).text(),
        title_id = title.replace(/ /g,'');
        title_id = title_id.replace('&', ''),
        menuitem = '<li class="component-tabbed-nav_li"><a href="#' + title_id + '" class="component-tabbed-nav__tab">' + title + '</a></li>';

    $('#menutest').append(menuitem);
    $(value).attr("id", title_id);
  });

  // $(window).bind('load', function() {
    var triggers = [].slice.call(document.querySelectorAll('.component-tabbed-nav_li')),
        highlight = document.createElement('span'),
        mobileOverlay = document.createElement('div'),
        bg = $('main.container').attr('data-bg');

    // Dynamically add background logo from data attribute
    $('head').append('<style>main.container::before { background-image: url("' + bg + '"); }</style>');

    // Create and add the Highlight Bar element
    highlight.classList.add('highlight');
    document.body.appendChild(highlight);

    // Create and add the Mobile Overlay element
    mobileOverlay.classList.add('mobileOverlay');
    document.body.appendChild(mobileOverlay);

    function highlightLink(e) {
      // Moves the highlight bar when focus changes
      e.preventDefault();

      var linkCoords = this.getBoundingClientRect(),
          coords = {
            width: linkCoords.width,
            height: linkCoords.height,
            top: linkCoords.top,
            left: linkCoords.left
          },
          thisLink = this.children[0];

      if (!$('.experience-editor').length) {
        highlight.style.transform = 'translate(' + (coords.left) + 'px, ' + (coords.top + (linkCoords.height + 8)) + 'px)';
        highlight.style.msTransform = 'translate(' + (coords.left) + 'px, ' + (coords.top + (linkCoords.height + 8)) + 'px)';
        highlight.style.width = coords.width + 'px';

        if (thisLink.hash !== "") {
          var hash = thisLink.hash,
              pageOffset = $(hash).offset().top;

          $('html').animate({
            scrollTop: pageOffset - 130
          }, 250, function() {
            // Not in requirements, and currently causes issues - consider removing
            // window.location.hash = hash;
          });

          mobileMenu.removeClass('active');
          $('body').removeClass('active');
          $('.mobileOverlay').removeClass('active');
          $('.component-tabbed-nav').removeClass('active');
        }
      };
    };

    triggers.forEach(function (a) {
      return a.addEventListener('click', highlightLink);
    });
  // });

  function mobileNav() {
    if (!$('.experience-editor').length) {
      mobileMenu.toggleClass('active');
      $('body').toggleClass('active');
      $('.mobileOverlay').toggleClass('active');
      $('.component-tabbed-nav').toggleClass('active');
    }
  }

  mobileMenu.on('click', mobileNav);

  window.addEventListener('load', function(){
    // Trigger first menu item
    $("#menutest .component-tabbed-nav_li:first-of-type").trigger('click');
    setTimeout(function() {
      $('.highlight').css('opacity', 1);
    }, 1000);
  });
})(window.jQuery, this, this.document);
